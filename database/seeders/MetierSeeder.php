<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MetierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        while ($i <= 10) {
            factory(Metier::class, 10)->create();
        }
    }
}
