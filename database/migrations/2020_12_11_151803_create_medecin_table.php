<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedecinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medecin', function (Blueprint $table) {
            $table->id();
            $table->string('nom', 50);
            $table->string('prenom', 30);
            $table->char('tel')->length(10);
            $table->char('mail', 50);
            $table->unsignedBigInteger('cabinet_id');
            $table->unsignedBigInteger('metier_id');
            $table->foreign('cabinet_id')->references('id')->on('cabinet')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('metier_id')->references('id')->on('metier')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medecin');
    }
}
