<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visite', function (Blueprint $table) {
            $table->id();
            $table->string('intitule', 255);
            $table->date('date');
            $table->string('horaire', 5);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('medecin_id');
            $table->unsignedBigInteger('medicament_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('medicament_id')->references('id')->on('medicament')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('medecin_id')->references('id')->on('medecin')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visite');
    }
}
