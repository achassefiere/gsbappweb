<?php

namespace Database\Factories;

use App\Models\Medecin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class MedecinFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medecin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->unique()->lastName,
            'prenom' => $this->faker->unique()->firstName,
            'tel' => $this->faker->randomNumber('0'+9),
            'mail' => $this->faker->unique()->email,
            'cabinet_id' => DB::table('cabinet')->select('id')->inRandomOrder()->first()->id,
            'metier_id' => DB::table('metier')->select('id')->inRandomOrder()->first()->id,
        ];
    }
}
