<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\defaultController;
use App\Http\Controllers\CabinetController;
use App\Http\Controllers\MedecinController;
use App\Http\Controllers\MedicamentController;
use App\Http\Controllers\VisiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MetierController;
use App\Http\Controllers\DepartementController;
use App\Http\Middleware\Admin;
use App\Http\Middleware\Secretaire;
use App\Http\Middleware\Visiteur;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { //la route au lancement de l'application
    return view('../auth/login');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
        ->name('home');         //on utilise une route nommée comme ca on peut changer la route sans changer son nom
Auth::routes();

//ROUTES DEPARTEMENT

Route::get('/region' , [DepartementController::class ,'show_region'])
        ->name('showRegion');
Route::get('/{region?}' , [DepartementController::class ,'show_departement'])
        ->name('showDepartement');

Route::prefix('departement')->group(
        function(){
                Route::get('/new_departement/{region?}' , [DepartementController::class ,'new_departement'])
        ->name('newDepartement');
                Route::post('/add_departement' , [DepartementController::class ,'add_departement'])
        ->name('addDepartement');
                Route::get('/edit_departement/{id}' , [DepartementController::class ,'edit_departement'])
        ->name('editDepartement');
                Route::post('/edit_departement/update_departement/{id}' , [DepartementController::class ,'update_departement'])
        ->name('updateDepartement');
                Route::get('/delete_departement/{id}' , [DepartementController::class ,'delete_departement'])
        ->name('deleteDepartement');
        }
);

//ROUTES CABINET

Route::prefix('cabinet')->group(
        function(){
                Route::get('/{id}' , [CabinetController::class ,'show_cabinet'])
        ->name('showCabinet');
                Route::get('/new_cabinet/{id}' , [CabinetController::class ,'new_cabinet'])
        ->name('newCabinet');
                Route::post('/add_cabinet' , [CabinetController::class ,'add_cabinet'])
        ->name('addCabinet');
                Route::get('/edit_cabinet/{id}' , [CabinetController::class ,'edit_cabinet'])
        ->name('editCabinet');
                Route::post('/edit_cabinet/update_cabinet/{id}' , [CabinetController::class ,'update_cabinet'])
        ->name('updateCabinet');
                Route::get('/delete_cabinet/{id}' , [CabinetController::class ,'delete_cabinet'])
        ->name('deleteCabinet');
        }
);

//ROUTES MEDECIN

Route::prefix('medecin')->group(
        function(){
                Route::get('/{id}' , [MedecinController::class ,'show_medecin'])
        ->name('showMedecin');
                Route::get('/profil_medecin/{id}' , [MedecinController::class ,'show_profil_medecin'])
        ->name('showProfilMedecin');
                Route::get('/new_medecin/{id}' , [MedecinController::class ,'new_medecin'])
        ->name('newMedecin');
                Route::post('/add_medecin' , [MedecinController::class ,'add_medecin'])
        ->name('addMedecin');
                Route::get('/edit_medecin/{id}' , [MedecinController::class ,'edit_medecin'])
        ->name('editMedecin');
                Route::post('/edit_medecin/update_medecin/{id}' , [MedecinController::class ,'update_medecin'])
        ->name('updateMedecin');
                Route::get('/delete_medecin/{id}' , [MedecinController::class ,'delete_medecin'])
        ->name('deleteMedecin');
        }
);


//ROUTES USER

Route::get('/home/user' , [UserController::class ,'show_user'])
        ->name('showUser');
Route::get('/home/profil_user/{id}' , [UserController::class ,'show_profil_user'])
        ->name('showProfilUser');

Route::prefix('user')->group(
        function(){
                Route::get('/new_user/{id}' , [UserController::class ,'new_user'])
        ->name('newUser');
                Route::post('/add_user' , [UserController::class ,'add_user'])
        ->name('addUser');
                Route::get('/edit_user/{id}' , [UserController::class ,'edit_user'])
        ->name('editUser');
                Route::post('/edit_user/update_user/{id}' , [UserController::class ,'update_user'])
        ->name('updateUser');
                Route::get('/delete_user/{id}' , [UserController::class ,'delete_user'])
        ->name('deleteUser');
        }
);


//ROUTES MEDICAMENT
Route::get('/home/medicament' , [MedicamentController::class ,'show_medicament'])
        ->name('showMedicament');


Route::prefix('medicament')->group(
        function(){
                Route::get('/info_medicament/{id}' , [MedicamentController::class ,'show_info_medicament'])
        ->name('showInfoMedicament');
                Route::get('/new_medicament' , [MedicamentController::class ,'new_medicament'])
        ->name('newMedicament');
                Route::post('/add_medicament' , [MedicamentController::class ,'add_medicament'])
        ->name('addMedicament');
                Route::get('/edit_medicament/{id}' , [MedicamentController::class ,'edit_medicament'])
        ->name('editMedicament');
                Route::post('/edit_medicament/update_medicament/{id}' , [MedicamentController::class ,'update_medicament'])
        ->name('updateMedicament');
                Route::get('/delete_medicament/{id}' , [MedicamentController::class ,'delete_medicament'])
        ->name('deleteMedicament');
        }
);

//ROUTES METIER

Route::prefix('metier')->group(
        function(){
                Route::get('/{id}' , [MetierController::class ,'show_metier'])
        ->name('showMetier');
                Route::get('/info_metier/{id}' , [MetierController::class ,'show_info_metier'])
        ->name('showProfilMetier');
                Route::get('/new_metier/{id}' , [MetierController::class ,'new_metier'])
        ->name('newMetier');
                Route::post('/add_metier' , [MetierController::class ,'add_metier'])
        ->name('addMetier');
                Route::get('/edit_metier/{id}' , [MetierController::class ,'edit_metier'])
        ->name('editMetier');
                Route::post('/edit_metier/update_metier/{id}' , [MetierController::class ,'update_metier'])
        ->name('updateMetier');
                Route::get('/delete_metier/{id}' , [MetierController::class ,'delete_metier'])
        ->name('deleteMetier');
        }
);

//ROUTES VISITE

Route::get('/home/all_visite' , [VisiteController::class ,'show_all_visite'])
        ->name('showAllVisite');

Route::prefix('visite')->group(
        function(){
                Route::get('/{id}' , [VisiteController::class ,'show_visite'])
        ->name('showVisite');
                Route::get('/new_visite/{id}' , [VisiteController::class ,'new_visite'])
        ->name('newVisite');
                Route::post('/add_visite' , [VisiteController::class ,'add_visite'])
        ->name('addVisite');
                Route::get('/edit_visite/{id}' , [VisiteController::class ,'edit_visite'])
        ->name('editVisite');
                Route::post('/edit_visite/update_visite/{id}' , [VisiteController::class ,'update_visite'])
        ->name('updateVisite');
                Route::get('/delete_visite/{id}' , [VisiteController::class ,'delete_visite'])
        ->name('deleteVisite');
        }
);

//ROUTES ADMIN

Route::group(['middleware' => 'admin'] , function () {
                Route::post('/add_departement' , [DepartementController::class ,'add_departement'])
        ->name('addDepartement');
                Route::get('/edit_departement/{id}' , [DepartementController::class ,'edit_departement'])
        ->name('editDepartement');
                Route::post('/edit_departement/update_departement/{id}' , [DepartementController::class ,'update_departement'])
        ->name('updateDepartement');
                Route::get('/delete_departement/{id}' , [DepartementController::class ,'delete_departement'])
        ->name('deleteDepartement');
                Route::get('/new_cabinet/{id}' , [CabinetController::class ,'new_cabinet'])
        ->name('newCabinet');
                Route::post('/add_cabinet' , [CabinetController::class ,'add_cabinet'])
        ->name('addCabinet');
                Route::get('/edit_cabinet/{id}' , [CabinetController::class ,'edit_cabinet'])
        ->name('editCabinet');
                Route::post('/edit_cabinet/update_cabinet/{id}' , [CabinetController::class ,'update_cabinet'])
        ->name('updateCabinet');
                Route::get('/delete_cabinet/{id}' , [CabinetController::class ,'delete_cabinet'])
        ->name('deleteCabinet');
                Route::get('/new_metier/{id}' , [MetierController::class ,'new_metier'])
        ->name('newMetier');
                Route::post('/add_metier' , [MetierController::class ,'add_metier'])
        ->name('addMetier');
                Route::get('/edit_metier/{id}' , [MetierController::class ,'edit_metier'])
        ->name('editMetier');
                Route::post('/edit_metier/update_metier/{id}' , [MetierController::class ,'update_metier'])
        ->name('updateMetier');
                Route::get('/delete_metier/{id}' , [MetierController::class ,'delete_metier'])
        ->name('deleteMetier');
                Route::get('/new_medicament' , [MedicamentController::class ,'new_medicament'])
        ->name('newMedicament');
                Route::post('/add_medicament' , [MedicamentController::class ,'add_medicament'])
        ->name('addMedicament');
                Route::get('/edit_medicament/{id}' , [MedicamentController::class ,'edit_medicament'])
        ->name('editMedicament');
                Route::post('/edit_medicament/update_medicament/{id}' , [MedicamentController::class ,'update_medicament'])
        ->name('updateMedicament');
                Route::get('/delete_medicament/{id}' , [MedicamentController::class ,'delete_medicament'])
        ->name('deleteMedicament');
});

//ROUTES SECRETAIRE

Route::group(['middleware' => 'secretaire'] , function () {

                Route::get('/home/all_visite' , [VisiteController::class ,'show_all_visite'])
        ->name('showAllVisite');
                Route::get('/new_medecin/{id}' , [MedecinController::class ,'new_medecin'])
        ->name('newMedecin');
                Route::post('/add_medecin' , [MedecinController::class ,'add_medecin'])
        ->name('addMedecin');
                Route::get('/edit_medecin/{id}' , [MedecinController::class ,'edit_medecin'])
        ->name('editMedecin');
                Route::post('/edit_medecin/update_medecin/{id}' , [MedecinController::class ,'update_medecin'])
        ->name('updateMedecin');
                Route::get('/delete_medecin/{id}' , [MedecinController::class ,'delete_medecin'])
        ->name('deleteMedecin');
});