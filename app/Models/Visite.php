<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visite extends Model
{
    use HasFactory;
    protected $table = 'visite';
    public $incrementing = true;

    protected $fillable = [
        'intitule', 
        'date',
        'horaire', 
        'user_id',
        'medecin_id',
        'medicament_id'
    ];

    public function medecin() {
        return $this->hasOne(Medecin::class, 'id', 'medecin_id');
    }

    public function medicament() {
        return $this->hasOne(Medicament::class, 'id', 'medicament_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
