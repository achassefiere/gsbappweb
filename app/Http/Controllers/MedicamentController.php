<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Models\Medicament;
use App\Models\Visite;


class MedicamentController extends Controller
{
    public function show_medicament() {

        $medicaments = Medicament::orderBy('nom')->get();
        return view("view.viewMedicament", ["medicaments" => $medicaments]);
    }

    public function show_info_medicament($id = null) {
        
        $medicament = Medicament::find($id);
        $visites = Visite::where('medicament_id', '=', $id)->get();
        return view("view.viewInfoMedicament", ["medicament" => $medicament,
                                                "visites" => $visites]);
    }

    public function new_medicament() {

        return view("new.newMedicament");
    }

    public function add_medicament(Request $request) {

        $medicament = new Medicament;
        $medicament->nom = $request->nom;
        $medicament->prix = $request->prix;
        $medicament->save();
        
        return redirect('/home/medicament');
    }

    public function edit_medicament($id) {

        $medicaments = Medicament::find($id);
        return view('update.updateMedicament',['medicaments'=>$medicaments]);

    }

    public function update_medicament(Request $request,$id) {

        $nom = $request->input('nom');
        $prix = $request->input('prix');
        
        DB::update('update medicament set nom = ?, prix = ?  where id = ?',
        [$nom, $prix, $id]);

        return redirect('/home/medicament');
    }

    public function delete_medicament($id) {

        DB::delete('delete from medicament where id = ?',[$id]);
        return redirect('/home/medicament');
    }
    
}