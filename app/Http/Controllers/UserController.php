<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Roles;
use App\Models\Visite;

class UserController extends Controller
{
    public function show_user() {

        $users = User::orderBy('role_id', 'ASC')->simplePaginate(10);
        return view("view.viewUser", ["users" => $users]);
        
    }

    public function show_profil_user($id = null) {
        
        $user = User::find($id);
        $visites = Visite::where('user_id', '=', $id)->get();
        return view("view.viewProfilUser", ["visites" => $visites,
                                                "user" => $user]);
    }

    public function edit_user($id) {
        
        $roles = Roles::All();
        $users = User::find($id);
        return view('update.updateUser',['users'=>$users,
                                        'roles'=>$roles]);

    }

    public function update_user(Request $request,$id) {

        $name = $request->input('name');
        $email = $request->input('email');
        $role_id = $request->input('role');
        DB::update('update users set name = ?, email = ?, role_id = ? where id = ?',
        [$name, $email, $role_id, $id]);

        return redirect('/home/user');
    }

    public function delete_user($id) {

        DB::delete('delete from users where id = ?',[$id]);
        return redirect('/home/user');
    }
    
}