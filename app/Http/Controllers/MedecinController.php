<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Medecin;
use App\Models\Cabinet;
use App\Models\Metier;
use App\Models\Visite;
use App\Models\User;


class MedecinController extends Controller
{

    public function show_medecin($cabinet = null) {
        $medecins = Medecin::where('cabinet_id', '=', $cabinet)->orderBy('nom', 'ASC')->get();
        $find_cabinet = Cabinet::find($cabinet);
        return view("view.viewMedecin", ["medecins" => $medecins,
                                            "cabinet" => $find_cabinet]);
    }

    public function show_profil_medecin($id = null) {
        
        $medecin = Medecin::find($id);
        $user = User::All();
        $visites = Visite::where('medecin_id', '=', $id)->get();
        return view("view.viewProfilMedecin", ["medecin" => $medecin,
                                                "visites" => $visites,
                                                "user" => $user]);
    }

    public function new_medecin($id) {
        $cabinet = Cabinet::find($id);
        $metiers = Metier::All();
        return view("new.newMedecin", ["cabinet" => $cabinet,
                                        "metiers" => $metiers]);
    }

    public function add_medecin(Request $request) {

        $medecin = new Medecin;
        $medecin->nom = $request->nom;
        $medecin->prenom = $request->prenom;
        $medecin->tel = $request->tel;
        $medecin->mail = $request->mail;
        $medecin->cabinet_id = $request->cabinet;
        $medecin->metier_id = $request->metier;
        $medecin->save();
        
        return redirect()->route('showMedecin', [$medecin->cabinet_id]);
    }

    public function edit_medecin($id) {

        $medecins = Medecin::find($id);
        $cabinets = Cabinet::All();
        $metiers = Metier::All();
        return view('update.updateMedecin',['cabinets'=> $cabinets,
                                            'medecins'=> $medecins,
                                            'metiers' => $metiers]);
    }

    public function update_medecin(Request $request,$id) {

        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $tel = $request->input('tel');
        $mail = $request->input('mail');
        $cabinet = $request->input('cabinet');
        $metier = $request->input('metier');
        
        DB::update('update medecin set nom = ?, prenom = ?, tel = ?, mail = ?, cabinet_id = ?, metier_id = ? where id = ?',
        [$nom, $prenom, $tel, $mail, $cabinet, $metier, $id]);

        return redirect()->route('showMedecin', [$cabinet]);
    }

    public function delete_medecin($id) {
        
        DB::delete('delete from medecin where id = ?',[$id]);

        return redirect()->back();
    }
    
}