<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Visite;
use App\Models\User;
use App\Models\Medecin;
use App\Models\Cabinet;
use App\Models\Medicament;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;



class VisiteController extends Controller
{
    public function show_visite() {
        $visites = Visite::where('user_id', '=', Auth::user()->id)->orderBy('date', 'ASC')->simplePaginate(10);
        return view("view.viewVisite", ["visites" => $visites]);
    }

    public function show_all_visite() {
        $visites = Visite::orderBy('date', 'ASC')->simplePaginate(10);
        return view("view.viewAllVisite", ["visites" => $visites]);
    }

    public function new_visite($id) {
        $cabinets = Cabinet::All();
        $users = User::All();
        $medecin = Medecin::find($id);
        $medicaments = Medicament::All();
        $visites = Visite::where('medecin_id', '=', $id)->get();
        return view("new.newVisite", ["cabinets" => $cabinets,
                                    "users" => $users,
                                    "medecin" => $medecin,
                                    "medicaments" => $medicaments,
                                    "visites" => $visites]);
    }

    public function add_visite(Request $request) {
        $visite = new Visite;
        $visite->intitule = $request->intitule;
        $visite->date = $request->date;
        $visite->horaire = $request->horaire;
        $visite->medecin_id = $request->medecin;
        $visite->medicament_id = $request->medicament;
        $visite->user_id = $request->visiteur;
        $listeVisitesMedecin = Visite::where('medecin_id', '=', $visite->medecin_id)->where('date', '=', $visite->date)->where('horaire', '=', $visite->horaire)->get(); //where('date', '=', $request->date && 'horaire', '=', $request->horaire)->get();
        if(count($listeVisitesMedecin) == 0) {
            $listeVisitesUser = Visite::where('user_id', '=', $visite->user_id)->where('date', '=', $visite->date)->where('horaire', '=', $visite->horaire)->get();
            if(count($listeVisitesUser) == 0) {
                $visite->save();
                return redirect()->route('showVisite', [Auth::user()->id]);
            }
            else{
                return redirect()->route('showVisite', [Auth::user()->id]);
            }
        }
        else{
            return redirect()->route('showVisite', [Auth::user()->id]);
        }
    }

    public function edit_visite($id) {
        $cabinets = Cabinet::All();
        $users = User::All();
        $medecins = Medecin::All();
        $medicaments = Medicament::All();
        $visites = Visite::find($id);
        return view('update.updateVisite',['visites'=>$visites,
                                            "cabinets" => $cabinets,
                                            "users" => $users,
                                            "medecins" => $medecins,
                                            "medicaments" => $medicaments]);

    }

    public function update_visite(Request $request,$id) {

        $intitule = $request->input('intitule');
        $date = $request->input('date');
        $horaire = $request->input('horaire');
        $visiteur = $request->input('visiteur');
        $medecin = $request->input('medecin');
        $medicament = $request->input('medicament');
        DB::update('update visite set intitule = ?, date = ?, horaire = ?, user_id = ?, medecin_id = ?, medicament_id = ? where id = ?',
        [$intitule, $date, $horaire, $visiteur, $medecin, $medicament, $id]);

        return redirect()->route('showVisite', [Auth::user()->id]);
    }

    public function delete_visite($id) {

        DB::delete('delete from visite where id = ?',[$id]);
        return redirect()->route('showVisite', [Auth::user()->id]);
    }
    
}