<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Models\Cabinet;
use App\Models\Departement;


class CabinetController extends Controller
{

    public function show_cabinet($departement = null) {
        
        $cabinets = Cabinet::where('departement_id', '=', $departement)->orderBy('nom', 'ASC')->simplePaginate(10);
        $find_departement = Departement::find($departement);
        return view("view.viewCabinet", ["departement" => $find_departement,
                                            "cabinets" => $cabinets]);
                                            
    }

    public function new_cabinet($id) {
        $departement = Departement::find($id);
        return view("new.newCabinet" , ["departement" => $departement]);
    }

    public function add_cabinet(Request $request) {

        $cabinet = new Cabinet;
        $cabinet->nom = $request->nom;
        $cabinet->codepostal = $request->codepostal;
        $cabinet->adresse = $request->adresse;
        $cabinet->ville = $request->ville;
        $cabinet->departement_id = $request->departement;
        $cabinet->save();
        
        return redirect()->route('showCabinet', [$cabinet->departement_id]);
    }

    public function edit_cabinet($id) {

        $cabinets = Cabinet::find($id);
        $departements = Departement::All();
        return view('update.updateCabinet',['cabinets'=>$cabinets,
                                            'departements'=>$departements]);

    }

    public function update_cabinet(Request $request,$id) {

        $nom = $request->input('nom');
        $codepostal = $request->input('codepostal');
        $adresse = $request->input('adresse');
        $ville = $request->input('ville');
        $departement = $request->input('departement');

        DB::update('update cabinet set nom = ?, codepostal = ?, adresse = ?, ville = ?, departement_id = ? where id = ?',
        [$nom, $codepostal, $adresse, $ville, $departement, $id]);

        return redirect()->route('showCabinet', [$departement]);
    }

    public function delete_cabinet($id) {

        DB::delete('delete from cabinet where id = ?',[$id]);
        
        return redirect()->back();
    }
}