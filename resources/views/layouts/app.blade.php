<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="/dashboard.css" rel="stylesheet">
        <link href="/profil.css" rel="stylesheet">
    </head>
    <body>
    <div id="app">

        <!-- Navbar -->
        <nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
            <!-- Partie gauche de la navbar -->
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        @guest
                            @if (Route::has('login'))
                                <a class="nav-link" href="{{route('home')}}">
                                    <img src="https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=120">
                                </a>
                            @endif

                            @else
                                <a class="nav-link" href="{{ route('login') }}">
                                    <img src="https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=120">
                                </a>
                        @endguest
                    </li>
                </ul>
            </div>
            

            <!-- Partie droite de la navbar -->
            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul class="navbar-nav ml-auto">
                    @guest
                        @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Connexion') }}</a>
                                </li>
                            @endif
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Inscription') }}</a>
                                </li>
                            @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" 
                                        class="btn btn-primary btn active">{{ __('Deconnexion') }}</a>        
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                    @endguest
                </ul>
            </div>
        </nav>
        <!-- End Navbar -->



        <!-- Sidebar -->
        @guest
            @if (Route::has('login'))
            @endif
            @else
                <div class="container-fluid">
                    <div class="row">
                        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                            <div class="sidebar-sticky">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}">
                                        <span data-feather="home"></span>
                                        Accueil
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                    @if (Auth::user()->role_id == 3)
                                    <a class="nav-link" href="{{ route('showVisite', [Auth::user()->id]) }}">
                                        <span data-feather="file"></span>
                                        Vos visites
                                        </a>
                                    </li>
                                    @endif

                                    @if (Auth::user()->role_id == 2)
                                    <a class="nav-link" href="{{ route('showAllVisite') }}">
                                        <span data-feather="file"></span>
                                        Gestion des visites médicales
                                        </a>
                                    </li>
                                    @endif

                                    @if (Auth::user()->role_id == 1)
                                    <a class="nav-link" href="{{ route('showUser') }}">
                                        <span data-feather="file"></span>
                                        Gestion des utilisateurs
                                        </a>
                                    </li>
                                    @endif
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('showProfilUser', [Auth::user()->id]) }}">
                                        <span data-feather="users"></span>
                                        Votre profil
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="">
                                        <span data-feather="flower"></span>
                                            {{ Auth::user()->role_id }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
        <!-- End Sidebar -->

                <!-- Bootstrap core JavaScript
                ================================================== -->
                <!-- Placed at the end of the document so the pages load faster -->
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
                <script src="../../assets/js/vendor/popper.min.js"></script>
                <script src="../../dist/js/bootstrap.min.js"></script>

                <!-- Icons -->
                <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
                <script>
                feather.replace()
                </script>

                <!-- Graphs -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
        @endguest

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>