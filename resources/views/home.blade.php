@extends('layouts.app')

@section('content')
    <div class="container-fluid">
      <div class="row">
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Bienvenue {{ Auth::user()->name }}</h1>
          </div>
          <div class="card">
            <div class="card-header">{{ __('Statut') }}</div>

            <div class="card-body">
              @if (session('status'))
                  <div class="alert alert-success" role="alert">
                      {{ session('status') }}
                  </div>
              @endif

              {{ __('Vous etes connecté !') }}
            </div>
        </main>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="card">
            <div class="card-header">
              {{ __('Prendre un rendez-vous') }}
            </div>
            <div class="card-body">
              <a href="{{ route('showRegion') }}" class="btn btn-white btn-block" style="background-color: #61a0ff; color: white;">Créer une visite</a>
            </div>
        </main>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="card">
            <div class="card-header">
              {{ __('Liste des medicaments') }}
            </div>
            <div class="card-body">
              <a href="{{ route('showMedicament') }}" class="btn btn-info btn-block" style="background-color: #61a0ff; color: white;">Consulter la liste des médicaments</a>
            </div>
        </main>
      </div>
    </div>
@endsection