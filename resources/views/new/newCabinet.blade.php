@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('addCabinet')}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire d'ajout d'un cabinet médical</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="nom">Nom</label>
                                <input class="form-control" name="nom" id="nom" placeholder="Nom du cabinet médical">
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="codepostal">Code Postal</label>
                                <input type="number" class="form-control" name="codepostal" id="codepostal" placeholder="Code Postal" value="{{ $departement->code }}">
                            </div>
                            <div class="form-group col-md">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Adresse du cabinet">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="departement">Departement</label>
                                <input type="text" class="form-control" name="departement" id="departement" value="{{ $departement->nom }}" readonly>
                                <input type="hidden" name="departement" value="{{ $departement->id }}">
                            </div>
                            <div class="form-group col-md">
                                <label for="Ville">Ville</label>
                                <input type="text" class="form-control" name="ville" id="Ville" placeholder="Ville du cabinet">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection