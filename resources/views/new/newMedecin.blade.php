@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('addMedecin', ['id' => $cabinet->id])}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire de création d'un médecin</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="Nom">Nom</label>
                                <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du médecin">
                            </div>
                            <div class="form-group col-md-5">
                                <label for="Prenom">Prénom</label>
                                <input type="text" class="form-control" name="prenom" id="Prenom" placeholder="Prénom du médecin">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="Mail">Adresse Mail</label>
                                <input type="text" class="form-control" name="mail" id="Mail" placeholder="Mail du médecin">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Tel">Numéro de télephone</label>
                                <input type="number" class="form-control" name="tel" id="Tel" placeholder="Télephone du médecin">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="metier">Profession</label>
                                <select type="number" name="metier" id="metier" class="form-control">
                                    @foreach($metiers as $metier)
                                        <option value="{{$metier->id}}">{{$metier->nom}}</option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group col-md">
                                <label for="Cabinet">Cabinet Médical</label>
                                <input type="text" class="form-control" name="cabinet" id="Cabinet" placeholder="Cabinet du médecin" value="{{$cabinet->nom}}" readonly>
                                <input type="hidden" name="cabinet" value="{{ $cabinet->id }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection