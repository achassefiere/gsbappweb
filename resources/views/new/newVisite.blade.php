@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('addVisite')}}">
                @csrf
                    <h3 class="text-center text" name="txt">Créer une nouvelle visite</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="Intitule">Intitulé de la visite</label>
                                <input type="text" class="form-control" name="intitule" id="Intitule" placeholder="Intitulé de la visite" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="visiteur">Visiteur</label>
                                <input type="text" class="form-control" name="nom_visiteur" id="visiteur" value="{{ Auth::user()->name }}" readonly>
                                <input type="hidden" name="visiteur" value="{{ Auth::user()->id }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Date">Date</label>
                                <input type="date" class="form-control" name="date" id="Date" placeholder="Date de la visite" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="horaire">Heure</label>
                                    <select type="text" name="horaire" id="horaire" class="form-control" required>
                                        <option selected>08h00</option>
                                        <option>08h30</option>
                                        <option>09h00</option>
                                        <option>09h30</option>
                                        <option>10h00</option>
                                        <option>10h30</option>
                                        <option>11h00</option>
                                        <option>11h30</option>
                                        <option>13h00</option>
                                        <option>13h30</option>
                                        <option>14h00</option>
                                        <option>14h30</option>
                                        <option>15h00</option>
                                        <option>15h30</option>
                                        <option>16h00</option>
                                        <option>16h30</option>
                                        <option>17h00</option>
                                        <option>17h30</option>
                                        <option>18h00</option>
                                        <option>18h30</option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="medecin">Medecin</label>
                                <input type="text" class="form-control" name="medecin" id="medecin" value="{{ $medecin->prenom }} {{ $medecin->nom }}" readonly>
                                <input type="hidden" name="medecin" value="{{ $medecin->id }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="medicament">Medicament</label>
                                <select type="number" name="medicament" id="medicament" class="form-control">
                                    @foreach($medicaments as $medicament)
                                        <option value="{{$medicament->id}}">{{$medicament->nom}}</option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                        <br>
                        <br>
                        <h5> Agenda du Medecin </h5>
                        <table class="table table-striped table-bordered table-hover">
                            <br>
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Horaire</th>
                                    <th scope="col">Intitule</th>
                                    <th scope="col">Visiteur</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($visites as $visite)
                                <tr>
                                    <td>{{$visite->date}}</td>
                                    <td>{{$visite->horaire}}</td>
                                    <td>{{$visite->intitule}}</td>
                                    <td>{{$visite->user->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection