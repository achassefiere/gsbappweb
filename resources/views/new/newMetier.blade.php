@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('addMetier')}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire d'ajout d'une profession</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="Nom">Nom</label>
                                <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom de la profession">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Tarif">Tarif de la consultation (en €)</label>
                                <input type="number" class="form-control" name="tarif" id="Tarif" placeholder="Tarif de la consultation">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection