@extends('layouts.app')

@section('content')
    <div class="container emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="profile-head">
                        <h5> {{$medicament->nom}} </h5>
                        <h6> {{$medicament->prix}} € </h6>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-img">
                        <img class="card-img-top" src="http://loremflickr.com/400/300/{{ $medicament->nom }}" alt="Card image" style="width:100%">
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-hover">
            <br>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Horaire</th>
                    <th scope="col">Intitule</th>
                    <th scope="col">Visiteur</th>
                </tr>
            </thead>

            <tbody>
            @foreach ($visites as $visite)
                <tr>
                    <td>{{$visite->date}}</td>
                    <td>{{$visite->horaire}}</td>
                    <td>{{$visite->intitule}}</td>
                    <td>{{$visite->user->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-warning"><a href="{{ route('showMedicament')}}">Retour</a></button>
    </div>
    
@endsection