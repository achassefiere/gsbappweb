@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <h1 class="text-center">Liste des Régions</h1>
                <br>
                <br>
                <div class="card-deck">
                    @foreach ($regions as $region)
                    <div class="card mb-5" style="flex: inherit;">
                        <img class="card-img-top" src="http://loremflickr.com/400/300/{{ $region->region }}" alt="Card image" style="width:100%">
                        <div class="card-body " style="text-align: center">
                            <a href="{{ route('showDepartement', [$region->region]) }}">{{$region->region}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </main>
        </div>
    </div>
@endsection