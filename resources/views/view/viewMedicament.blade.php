@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="container col-md-11">
                    <h1 class="text-center">Liste des médicaments à présenter</h1>
                    <br>
                    <br>
                    <div class="card-deck">
                        @foreach ($medicaments as $medicament)
                        <div class="card mb-5" style="flex: inherit; padding: 10px; background: #eee;">
                            <img class="card-img-top" src="http://loremflickr.com/300/200/{{ $medicament->nom }}" alt="Card image" style="width:100%">
                            <div class="card-body" style="text-align: center">
                                <a href="{{ route('showInfoMedicament', [$medicament->id]) }}" style="font-size: 20px; color: black;">{{$medicament->nom}}</a>
                                <br>
                                <br>
                                @if (Auth::user()->role_id == 1)
                                <a href="{{ route('editMedicament', [$medicament->id]) }}" class="btn btn-warning btn-block">Modifier</a>
                                <a href="{{ route('deleteMedicament', [$medicament->id]) }}" class="btn btn-danger btn-block" style="color: black;">Supprimer</a>
                                @endif
                            </div>
                        </div>
                        @endforeach

                        @if (Auth::user()->role_id == 1)
                        <div class="card mb-5" style="flex: inherit; padding: 10px; background: #eee;">
                            <img class="card-img-top" src="http://loremflickr.com/300/200/drug" alt="Card image" style="width:100%">
                            <div class="card-body">
                                <div class="card-title">
                                    <a class="btn btn-info btn-block" href="{{ route('newMedicament') }}">Ajouter Medicament</a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </main>
        </div>    
    </div>
@endsection