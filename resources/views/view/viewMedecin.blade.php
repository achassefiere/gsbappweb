@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="container col-md-11">
                <h1 class="text-center">Médecins du cabinet {{$cabinet->nom}}</h1>
                <br>
                <br>
                <div class="card-deck">
                    @foreach ($medecins as $medecin)
                    <div class="card mb-5" style="flex: inherit; padding: 10px; background: #eee;">
                        <img class="card-img-top" src="http://loremflickr.com/300/200/user" alt="Card image" style="width:100%">
                        <div class="card-body" style="text-align: center">
                            <div class="card-title">
                                <a class="text-dark" href="{{ route('showProfilMedecin', [$medecin->id]) }}">{{$medecin->prenom}} {{$medecin->nom}}</a>
                            </div>
                            <br>
                            <a class="btn btn-success btn-block" href="{{ route('newVisite', [$medecin->id]) }}" style="width:100%;">Créer une visite</a>

                            @if (Auth::user()->role_id == 1)
                            <a class="btn btn-warning btn-block" href="{{ route('editMedecin', [$medecin->id]) }}" style="width:100%;">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pen" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: black">
                            <path fill-rule="evenodd" d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                            </svg>Modifier</a>

                            <a class="btn btn-danger btn-block" href="{{ route('deleteMedecin', [$medecin->id]) }}" style="width:100%;">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="color: white">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                            </svg>Supprimer</a>
                            @endif
                        </div>
                    </div>
                    @endforeach

                    @if (Auth::user()->role_id == 1)
                    <div class="card mb-5" style="flex: inherit; padding: 10px; background: #eee;">
                        <img class="card-img-top" src="http://loremflickr.com/300/200/user" alt="Card image" style="width:100%">
                        <div class="card-body">
                            <div class="card-title">
                                <a class="btn btn-info btn-block" href="{{ route('newMedecin', [$cabinet->id]) }}">Ajouter Medecin</a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </main>
    </div>
</div>
@endsection