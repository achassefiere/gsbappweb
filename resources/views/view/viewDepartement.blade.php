@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <h1 class="text-center">Choisissez le département ou se situe le Medecin</h1>
                <br>
                <br>
                <div class="card-deck">
                    @foreach ($departements as $departement)
                    <div class="card mb-5" style="flex: inherit;">
                        <img class="card-img-top" src="http://loremflickr.com/300/200/{{ $departement->nom }}" alt="Card image" style="width:100%">
                        <div class="card-body " style="text-align: center">
                            <a href="{{ route('showCabinet', [$departement->id]) }}">{{$departement->nom}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </main>
        </div>    
    </div>
@endsection