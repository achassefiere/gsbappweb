@extends('layouts.app')

@section('content')
    <div class="container emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="profile-head">
                        <h5> {{ Auth::user()->name }} </h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-img">
                        <img src="https://www.svgrepo.com/show/36727/user.svg" alt=""/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nom</label>
                                </div>
                                <div class="col-md-6">
                                    <p> {{ Auth::user()->name }} </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p> {{ Auth::user()->email }} </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Role</label>
                                </div>
                                <div class="col-md-6">
                                    <p> {{ Auth::user()->role_id }} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-hover">
            <br>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Horaire</th>
                    <th scope="col">Intitule</th>
                    <th scope="col">Visiteur</th>
                </tr>
            </thead>

            <tbody>
            @foreach ($visites as $visite)
                <tr>
                    <td>{{$visite->date}}</td>
                    <td>{{$visite->horaire}}</td>
                    <td>{{$visite->intitule}}</td>
                    <td>{{$visite->user->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-warning"><a href="{{ route('home')}}">Retour</a></button>
    </div>
    
@endsection