@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('updateVisite', ['id' => $visites->id])}}">
                @csrf
                    <h3 class="text-center text" name="txt">Modifier une visite</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="Intitule">Intitulé de la visite</label>
                                <input type="text" class="form-control" name="intitule" id="Intitule" placeholder="Intitulé de la visite" value="{{$visites->intitule}}" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="visiteur">Visiteur</label>
                                <input type="text" class="form-control" name="nom_visiteur" id="visiteur" value="{{$visites->user->name}}" readonly>
                                <input type="hidden" name="visiteur" value="{{$visites->user_id}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Date">Date</label>
                                <input type="date" class="form-control" name="date" id="Date" placeholder="Date de la visite" value="{{$visites->date}}" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="horaire">Heure</label>
                                    <select type="text" name="horaire" id="horaire" class="form-control" required>
                                        <option selected>{{$visites->horaire}}</option>
                                        <option>08h00</option>
                                        <option>09h00</option>
                                        <option>10h00</option>
                                        <option>11h00</option>
                                        <option>13h00</option>
                                        <option>14h00</option>
                                        <option>15h00</option>
                                        <option>16h00</option>
                                        <option>17h00</option>
                                        <option>18h00</option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="medecin">Medecin</label>
                                <select type="number" name="medecin" id="medecin" class="form-control">
                                    <option selected value="{{$visites->medecin->id}}">{{$visites->medecin->nom}} {{$visites->medecin->prenom}}</option>
                                    @foreach($medecins as $medecin)
                                    <option value="{{$medecin->id}}">{{$medecin->nom}} {{$medecin->prenom}}</option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group col-md-4">
                                <label for="medicament">Medicament</label>
                                <select type="number" name="medicament" id="medicament" class="form-control">
                                    <option selected value="{{$visites->medicament->id}}">{{$visites->medicament->nom}}</option>
                                    @foreach($medicaments as $medicament)
                                    <option value="{{$medicament->id}}">{{$medicament->nom}}</option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>
    </div>
@endsection