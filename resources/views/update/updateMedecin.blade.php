@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('updateMedecin', ['id' => $medecins->id])}}">
                    @csrf
                    <h3 class="text-center text" name="txt">Formulaire de modification du medecin {{$medecins->nom}}</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="hidden" class="form-control" id="id" value="{{$medecins->id}}" readonly="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Prenom">Prénom</label>
                                <input type="text" class="form-control" name="prenom" id="Prenom" placeholder="Prénom du médecin" value="{{$medecins->prenom}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="Nom">Nom</label>
                                <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du médecin" value="{{$medecins->nom}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="Mail">Adresse Mail</label>
                                <input type="text" class="form-control" name="mail" id="Mail" placeholder="Mail du médecin" value="{{$medecins->mail}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Tel">Numéro de télephone</label>
                                <input type="number" class="form-control" name="tel" id="Tel" placeholder="Télephone du médecin" value="{{$medecins->tel}}">
                            </div>
                        </div>                      
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="Metier">Profession</label>
                                <select type="number" name="metier" id="Metier" class="form-control">
                                    <option selected value="{{$medecins->metier->id}}">{{$medecins->metier->nom}}</option>
                                    @foreach($metiers as $metier)
                                        <option value="{{$metier->id}}">{{$metier->nom}}</option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group col-md">
                                <label for="Cabinet">Cabinet Médical</label>
                                <select type="number" name="cabinet" id="Cabinet" class="form-control">
                                    <option selected value="{{$medecins->cabinet->id}}">{{$medecins->cabinet->nom}}</option>
                                    @foreach($cabinets as $cabinet)
                                        <option value="{{$cabinet->id}}">{{$cabinet->nom}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>
    </div>
@endsection
