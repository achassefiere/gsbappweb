@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="post" action="{{route('updateDepartement', ['id' => $departements->id])}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire de modification d'un département</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="Id">Id</label>
                                <input class="form-control" name="id" id="Id" placeholder="Un id va vous être attribué" readonly="">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="Code">Code</label>
                                <input type="number" class="form-control" name="code" id="Code" placeholder="Code du département" value="{{$departements->code}}">
                            </div>
                            <div class="form-group col-md">
                                <label for="Nom">Nom</label>
                                <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du département" value="{{$departements->nom}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="Region">Région</label>
                                <input type="text" class="form-control" name="region" id="Region" placeholder="Nom de la région" value="{{$departements->region}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection