@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="POST" action="{{route('updateUser', ['id' => $users->id])}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire de modification d'un utilisateur</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="name">Nom</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nom de l'utilisateur" value="{{$users->name}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Email de l'utilisateur" value="{{$users->email}}">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="role">Role</label>
                                <select type="number" name="role" id="role" class="form-control">
                                    <option selected value="{{$users->role_id}}">{{$users->role_id}}</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->id}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>
    </div>
@endsection