@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form id="login-form" class="form" method="post" action="{{route('updateCabinet', ['id' => $cabinets->id])}}">
                @csrf
                    <h3 class="text-center text" name="txt">Formulaire de modification du cabinet {{$cabinets->nom}}</h3>
                    <div class="container">
                        <div class="form-row">
                            <div class="form-group col-md">
                                <input type="hidden" class="form-control" id="id" value="{{$cabinets->id}}" readonly="">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nom">Nom</label>
                                <input type="text" class="form-control" name="nom" id="nom" value="{{$cabinets->nom}}">
                            </div>
                        </div>
                        
                        
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="codepostal">Code Postal</label>
                                <input type="number" class="form-control" name="codepostal" id="codepostal" placeholder="Code Postal" value="{{$cabinets->codepostal}}">
                            </div>
                            <div class="form-group col-md">
                                <label for="adresse">Adresse</label>
                                <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Adresse du cabinet" value="{{$cabinets->adresse}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md">
                                <label for="ville">Ville</label>
                                <input type="text" class="form-control" name="ville" id="ville" placeholder="Ville du cabinet" value="{{$cabinets->ville}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="departement">Département</label>
                                <select type="number" name="departement" id="departement" class="form-control">
                                    <option selected value="{{$cabinets->departement->id}}">{{$cabinets->departement->nom}}</option>
                                    @foreach($departements as $departement)
                                        <option value="{{$departement->id}}">{{$departement->nom}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                                
                                <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                                
                                <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>    
    </div>
@endsection